package com.xisfo.client.handlers;

import com.xisfo.client.models.Employee;
import com.xisfo.client.segurity.PBKDF2Encoder;
import com.xisfo.client.services.Employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.validation.Validator;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;


@RefreshScope
@Component
public class EmployeeHandler {

    @Autowired
    private Environment environment;

    @Autowired
    private EmployeeService service;

    @Autowired
    private Validator validator;

    @Autowired
    private PBKDF2Encoder passwordEncoder;


    public static  String url_employee =  "/api/xisfoclient/v1/employee/";

    public Mono<ServerResponse> list(ServerRequest request){
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(), Employee.class);
    }


    public Mono<ServerResponse> show(ServerRequest request){
        String id =  request.pathVariable("id");
        return service.findById(id).flatMap(p -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }


    public Mono<ServerResponse> create(ServerRequest request){
        Mono<Employee> newEmployee =  request.bodyToMono(Employee.class);

        return newEmployee.flatMap(e -> {
            Errors errors = new BeanPropertyBindingResult(e, Employee.class.getName());
            validator.validate(e, errors);

            if(errors.hasErrors()){
                return Flux.fromIterable(errors.getFieldErrors())
                        .map(fieldError -> "Input: " +  fieldError.getField() + " " + fieldError.getDefaultMessage())
                        .collectList()
                        .flatMap(list -> ServerResponse.badRequest().body(fromValue(list)));
            }else{
                if(e.getCreateAt() == null){
                    e.setCreateAt(new Date());
                }

                e.setPassword(passwordEncoder.encode(e.getPassword()));
                return service.save(e).flatMap(edb -> ServerResponse
                        .created(URI.create(url_employee.concat(edb.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(edb))
                );
            }
        });
    }


    public Mono<ServerResponse> update(ServerRequest request){
        Mono<Employee> employee =  request.bodyToMono(Employee.class);
        String id = request.pathVariable("id");

        Mono<Employee> employeeDB = service.findById(id);
        return employeeDB.zipWith(employee,(db, req) ->{
            db.setFirst_name(req.getFirst_name());
            db.setSecond_name(req.getSecond_name());
            db.setLast_name(req.getLast_name());
            db.setSecond_last_name(req.getSecond_last_name());
            db.setAdmission_date(req.getAdmission_date());
            db.setBirthday(req.getBirthday());
            db.setSons(req.getSons());

            return db;
        }).flatMap(empl -> ServerResponse.created(URI.create(url_employee.concat(empl.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.save(empl), Employee.class)
        ).switchIfEmpty(ServerResponse.notFound().build());

    }

    public Mono<ServerResponse> delete(ServerRequest request){
        String id =  request.pathVariable("id");
        Mono<Employee> employeeDB = service.findById(id);
        return employeeDB.flatMap(empl -> service.delete(empl).then(ServerResponse.noContent().build()))
                .switchIfEmpty(ServerResponse.notFound().build());
    }


    public Mono<ServerResponse> user(ServerRequest request) {
        Map<String, String> json =  new HashMap<>();

        if(environment.getActiveProfiles().length > 0 && environment.getActiveProfiles()[0].equals("dev")){
            json.put("autor.nombre", environment.getProperty("configuracion.autor.nombre"));
            json.put("autor.email", environment.getProperty("configuracion.autor.email"));
        }
        return ServerResponse.ok().bodyValue(json);
    }


}

package com.xisfo.client.handlers;

import com.xisfo.client.models.Client;
import com.xisfo.client.models.Employee;
import com.xisfo.client.services.Client.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Date;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

public class ClientsHandler {

    @Autowired
    private ClientService service;

    @Autowired
    private Validator validator;

    public static  String url_client =  "/api/xisfoclient/v1/client/";

    public Mono<ServerResponse> list(){
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(), ClientsHandler.class);
    }

    public Mono<ServerResponse> show(ServerRequest request){
        String id =  request.pathVariable("id");
        return service.findById(id).flatMap(p -> ServerResponse
                        .ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(p)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> create(ServerRequest request){
        Mono<Client> newClient =  request.bodyToMono(Client.class);

        return newClient.flatMap(e -> {
            Errors errors = new BeanPropertyBindingResult(e, Client.class.getName());
            validator.validate(e, errors);

            if(errors.hasErrors()){
                return Flux.fromIterable(errors.getFieldErrors())
                        .map(fieldError -> "Input: " +  fieldError.getField() + " " + fieldError.getDefaultMessage())
                        .collectList()
                        .flatMap(list -> ServerResponse.badRequest().body(fromValue(list)));
            }else{
                if(e.getCreateAt() == null){
                    e.setCreateAt(new Date());
                }

                return service.save(e).flatMap(edb -> ServerResponse
                        .created(URI.create(url_client.concat(edb.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(fromValue(edb))
                );
            }
        });
    }

    public Mono<ServerResponse> update(ServerRequest request){
        Mono<Client> client =  request.bodyToMono(Client.class);
        String id = request.pathVariable("id");

        Mono<Client> clientDB = service.findById(id);
        return clientDB.zipWith(client,(db, req) ->{
            db.setFirst_name(req.getFirst_name());

            return db;
        }).flatMap(empl -> ServerResponse.created(URI.create(url_client.concat(empl.getId())))
                .contentType(MediaType.APPLICATION_JSON)
                .body(service.save(empl), Client.class)
        ).switchIfEmpty(ServerResponse.notFound().build());

    }

    public Mono<ServerResponse> delete(ServerRequest request){
        String id =  request.pathVariable("id");
        Mono<Client> clientDB = service.findById(id);
        return clientDB.flatMap(empl -> service.delete(empl).then(ServerResponse.noContent().build()))
                .switchIfEmpty(ServerResponse.notFound().build());
    }



}

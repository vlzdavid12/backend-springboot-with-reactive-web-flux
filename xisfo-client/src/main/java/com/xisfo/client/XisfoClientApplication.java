package com.xisfo.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@RefreshScope
@EnableDiscoveryClient
@EnableEurekaClient
@SpringBootApplication
public class XisfoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(XisfoClientApplication.class, args);
    }

}

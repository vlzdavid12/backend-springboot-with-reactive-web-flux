package com.xisfo.client.models;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}

package com.xisfo.client.models;

import com.xisfo.client.segurity.PBKDF2Encoder;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Date;
import java.util.List;

@Document(collection = "employees")
public class Employee {
    @Id
    private String id;

    @NotEmpty(message = "Input username is required")
    private String username;

    @NotEmpty(message = "Input password is required")
    private String password;

    @NotEmpty(message = "Input first_name is required")
    private String first_name;

    @NotEmpty(message = "Input second_name is required")
    private String second_name;

    @NotEmpty(message = "Input last_name is required")
    private String last_name;

    @NotEmpty(message = "Input second_last_name is required")
    private String second_last_name;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date admission_date;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @Min(value = 0, message = "Only positive numbers are accepted")
    @Max(value = 12, message = "Maximum quantity is 12")
    private int sons;

    private Boolean enabled;

    private List<Role> roles;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createAt;

    public Employee(String username, String password, String first_name, String second_name, String last_name, String second_last_name, Date admission_date, Date birthday, int sons, Boolean enabled, List<Role> roles, Date createAt) {
        this.username = username;
        this.password = password;
        this.first_name = first_name;
        this.second_name = second_name;
        this.last_name = last_name;
        this.second_last_name = second_last_name;
        this.admission_date = admission_date;
        this.birthday = birthday;
        this.sons = sons;
        this.enabled = enabled;
        this.roles = roles;
        this.createAt = createAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getSecond_name() {
        return second_name;
    }

    public void setSecond_name(String second_name) {
        this.second_name = second_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getSecond_last_name() {
        return second_last_name;
    }

    public void setSecond_last_name(String second_last_name) {
        this.second_last_name = second_last_name;
    }

    public Date getAdmission_date() {
        return admission_date;
    }

    public void setAdmission_date(Date admission_date) {
        this.admission_date = admission_date;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getSons() {
        return sons;
    }

    public void setSons(int sons) {
        this.sons = sons;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }
}

package com.xisfo.client.dao;

import com.xisfo.client.models.Employee;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface EmployeeDao extends ReactiveMongoRepository<Employee, String> {
}

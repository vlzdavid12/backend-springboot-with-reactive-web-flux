package com.xisfo.client.dao;

import com.xisfo.client.models.Client;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface ClientDao extends ReactiveMongoRepository<Client, String> {
}

package com.xisfo.client.services.Employee;

import com.xisfo.client.dao.EmployeeDao;
import com.xisfo.client.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EmployeeServiceImpl implements  EmployeeService{
    @Autowired
    private EmployeeDao employeeDao;


    @Override
    public Flux<Employee> findAll() {
        return employeeDao.findAll();
    }

    @Override
    public Mono<Employee> findById(String id) {
        return employeeDao.findById(id);
    }

    @Override
    public Mono<Employee> save(Employee employee) {
        return employeeDao.save(employee);
    }

    @Override
    public Mono<Void> delete(Employee employee) {
        return employeeDao.delete(employee);
    }

}

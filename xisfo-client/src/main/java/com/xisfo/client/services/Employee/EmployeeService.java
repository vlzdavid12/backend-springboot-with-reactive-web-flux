package com.xisfo.client.services.Employee;

import com.xisfo.client.models.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EmployeeService {
    public Flux<Employee> findAll();

    public Mono<Employee> findById(String id);

    public Mono<Employee> save(Employee employee);

    public Mono<Void> delete(Employee employee);

}

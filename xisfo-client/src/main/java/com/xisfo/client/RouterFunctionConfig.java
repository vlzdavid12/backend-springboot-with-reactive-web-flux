package com.xisfo.client;

import com.xisfo.client.handlers.EmployeeHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


@Configuration
public class RouterFunctionConfig {


    @Bean
    public RouterFunction<ServerResponse> routerFunction(EmployeeHandler handler) {
        return RouterFunctions.route()
                .GET("/api/xisfoclient/v1/users", handler::user)
                .GET("/api/xisfoclient/v1/employees", handler::list)
                .GET("/api/xisfoclient/v1/employee/{id}", handler::show)
                .DELETE("/api/xisfoclient/v1/employee/{id}", handler::delete)
                .POST("/api/xisfoclient/v1/employee", handler::create)
                .build();
    }

}


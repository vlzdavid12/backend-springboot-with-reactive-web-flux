package com.example.xisfopay.handlers;

import com.example.xisfopay.models.Employee;
import com.example.xisfopay.services.Employee.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class EmployeeHandler {


    @Autowired
    private EmployeeService service;


    public Mono<ServerResponse> list(ServerRequest response) {
        // Propagate Token WebClient
        String token = response.headers().firstHeader(HttpHeaders.AUTHORIZATION);
        return ServerResponse.ok().contentType(MediaType.APPLICATION_JSON)
                .body(service.findAll(token), Employee.class);

    }

    public Mono<ServerResponse> show(ServerRequest request) {
        // Propagate Token WebClient
        String token = request.headers().firstHeader(HttpHeaders.AUTHORIZATION);
        String id = request.pathVariable("id");
        return errorHandler(
                service.findById(id, token).flatMap(p -> ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(p)).switchIfEmpty(ServerResponse.notFound().build())

        );
    }

    public Mono<ServerResponse> create(ServerRequest request) {
        Mono<Employee> employee = request.bodyToMono(Employee.class);
        String token = request.headers().firstHeader(HttpHeaders.AUTHORIZATION);
        return employee.flatMap(p -> {
                    if (p.getCreateAt() == null) {
                        p.setCreateAt(new Date());
                    }
                    return service.save(p, token);
                }).flatMap(p -> ServerResponse.created(URI.create("/api/xisfoclient/v1/employee/".concat(p.getId())))
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(p))
                .onErrorResume(error -> {
                    WebClientResponseException errorResponse = (WebClientResponseException) error;
                    if (errorResponse.getStatusCode() == HttpStatus.BAD_REQUEST) {
                        return ServerResponse.badRequest()
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(errorResponse.getResponseBodyAsByteArray());
                    }
                    return Mono.error(errorResponse);
                });
    }

    public Mono<ServerResponse> update(ServerRequest request) {
        Mono<Employee> employee = request.bodyToMono(Employee.class);
        String token = request.headers().firstHeader(HttpHeaders.AUTHORIZATION);
        String id = request.pathVariable("id");
        return errorHandler(
                employee
                        .flatMap(p -> service.update(p, id, token))
                        .flatMap(p -> ServerResponse.created(URI.create("/api/xisfoclient/v1/employee/".concat(p.getId())))
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(p))
        );
    }

    private Mono<ServerResponse> errorHandler(Mono<ServerResponse> response) {
        return response.onErrorResume(error -> {
            WebClientResponseException errorResponse = (WebClientResponseException) error;

            if (errorResponse.getStatusCode() == HttpStatus.NOT_FOUND) {
                Map<String, Object> body = new HashMap();
                body.put("Error", "No exist employee ".concat(errorResponse.getMessage()));
                body.put("timestamp", new Date());
                body.put("status", errorResponse.getStatusCode().value());
                return ServerResponse.status(HttpStatus.NOT_FOUND).bodyValue(body);
            }
            return Mono.error(errorResponse);

        });
    }

}

package com.example.xisfopay;

import com.example.xisfopay.handlers.EmployeeHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


@Configuration
public class RouterFunctionConfig {


    @Bean
    public RouterFunction<ServerResponse> routerFunction(EmployeeHandler handler) {
        return RouterFunctions.route()
                .GET("/api/xisfopay/v1/employees", handler::list)
                .GET("/api/xisfopay/v1/employee/{id}", handler::show)
                .POST("/api/xisfopay/v1/employee/", handler::create)
                .PUT("/api/xisfopay/v1/employee/{id}", handler::update)
                .build();
    }

}


package com.example.xisfopay.services.Employee;

import com.example.xisfopay.models.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.reactive.function.BodyInserters.fromValue;

@Service
public class EmployeeServiceImpl implements  EmployeeService {

    @Autowired
    private WebClient client;

    @Override
    public Flux<Employee> findAll(String token) {

        return client.get().uri("/api/xisfoclient/v1/employees/")
                .accept(MediaType.APPLICATION_JSON)
                .header("Authorization", token )
                .retrieve()
                .bodyToFlux(Employee.class);
    }



    @Override
    public Mono<Employee> findById(String id, String token) {
         Map<String, Object> params =  new HashMap<String, Object>();
         params.put("id", id);

         return client.get()
                 .uri("/api/xisfoclient/v1/employee/{id}", params)
                 .accept(MediaType.APPLICATION_JSON)
                 .header("Authorization", token )
                 .retrieve()
                 .bodyToMono(Employee.class);
                //.exchange()
                //.flatMap(response -> response.bodyToMono(Employee.class))

    }

    @Override
    public Mono<Employee> save(Employee employee, String token) {
        return  client.post()
                .uri("/api/xisfoclient/v1/employee/")
                .header("Authorization", token )
                .accept(MediaType.APPLICATION_JSON)
                .body(fromValue(employee))
                .retrieve()
                .bodyToMono(Employee.class);
    }


    @Override
    public Mono<Employee> update(Employee employee, String id, String token) {

        return client.put()
                .uri("/api/xisfoclient/v1/employee/{id}", Collections.singletonMap("id", id))
                .header("Authorization", token )
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(employee)
                .retrieve()
                .bodyToMono(Employee.class);
    }

    @Override
    public Mono<Void> delete(String id) {
        return null;
    }
}

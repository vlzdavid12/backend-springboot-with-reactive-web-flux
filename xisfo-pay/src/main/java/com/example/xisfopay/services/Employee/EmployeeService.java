package com.example.xisfopay.services.Employee;

import com.example.xisfopay.models.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface EmployeeService {
    public Flux<Employee> findAll(String token);
    public Mono<Employee> findById(String id, String token);
    public Mono<Employee> save(Employee employee, String token);
    public Mono<Employee> update(Employee employee, String id, String token);
    public Mono<Void> delete(String id);
}

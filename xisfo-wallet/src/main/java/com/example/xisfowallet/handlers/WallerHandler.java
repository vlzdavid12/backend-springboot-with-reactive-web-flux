package com.example.xisfowallet.handlers;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;


@RestController
public class WallerHandler {

    public Mono<ServerResponse> user(ServerRequest request) {
        return ServerResponse.ok().bodyValue("Paso parte del usuario Wallet!");
    }


    public Mono<ServerResponse> admin(ServerRequest request) {
        return ServerResponse.ok().bodyValue("Paso parte del administrador Wallet!");
    }
}

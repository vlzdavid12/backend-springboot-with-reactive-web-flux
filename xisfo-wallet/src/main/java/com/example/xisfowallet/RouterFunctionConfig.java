package com.example.xisfowallet;

import com.example.xisfowallet.handlers.WallerHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerResponse;


@Configuration
public class RouterFunctionConfig {


    @Bean
    public RouterFunction<ServerResponse> routerFunction(WallerHandler handler) {
        return RouterFunctions.route()
                .GET("/api/xisfowallet/v1/users", handler::user)
                .GET("/api/xisfowallet/v1/admin", handler::admin).build();
    }

}


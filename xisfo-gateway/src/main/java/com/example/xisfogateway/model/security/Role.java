package com.example.xisfogateway.model.security;

public enum Role {
    ROLE_USER, ROLE_ADMIN
}

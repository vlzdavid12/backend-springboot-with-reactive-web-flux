package com.example.xisfogateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class XisfoGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(XisfoGatewayApplication.class, args);
    }

}

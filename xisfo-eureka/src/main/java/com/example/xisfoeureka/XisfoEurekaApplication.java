package com.example.xisfoeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class XisfoEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(XisfoEurekaApplication.class, args);
    }

}
